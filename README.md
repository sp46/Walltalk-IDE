# Walltalk-IDE
![GitHub license](https://img.shields.io/github/license/sp45/Walltalk-IDE.svg)  

The Walltalk IDE is an open source IDE written in the JPHP language. It is very easy to write additions.


1. Install **Java Development Kit 9 or later** and the last version of [JPPM](https://github.com/jphp-group/jphp/releases).
2. Prepare IDE dependencies:
```bash
jppm prepare-ide
```

### Running the IDE

```bash
jppm start-ide
```

### Building the IDE

```bash
jppm build-ide
```

The build of IDE will be in the `ide/build` directory.

### IDE Sandbox [?]

```bash
jppm start-sandbox
``` 
